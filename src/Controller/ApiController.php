<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(HttpClientInterface $httpClient)
    {
        $response = $httpClient->request('GET', 'https://api.github.com/users/ismail1432/repos', [
            'query'=>[
                'sort'=>'created'
            ]
        ]);

        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
            'repos' =>$response->toArray(),
        ]);
    }

    /**
     * @Route("/api/show/{id}", name="api_show")
     */
    public function show($id, HttpClientInterface $httpClient)
    {
        $response = $httpClient->request('GET', 'https://api.github.com/repositories/'.$id);

        if( $response->getStatusCode() === Response::HTTP_NOT_FOUND){
            throw  new NotFoundHttpException('No repo with this id!');
        }

        return $this->render('api/show.html.twig', [
            'repo' =>$response->toArray(),
        ]);
    }
}
